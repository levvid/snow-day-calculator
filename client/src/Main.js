import React, { Component } from "react";
import {
  Route,
  Switch,
  BrowserRouter as Router,
} from "react-router-dom";
import Home from "./Home";
import Blog from "./views/BlogPage/BlogPage"
import styles from "../../client/src/assets/css/main.css";


class Main extends Component {
  render() {
    return (
      <Router>
        <div className={styles.body}>
          <Switch> 
            <Route exact path="/" component={Home}/>
            <Route path="/blog" component={Blog}/>
            <Route path="/blog/contact-us" component={Blog}/>
          </Switch>
        </div>
      </Router>
    );
  }
}
 
export default Main;