import { title } from "../../main-theme.js";

const productStyle = {
  section: {
    padding: "70px 0",
    textAlign: "center",
  },
  title: {
    ...title,
    marginBottom: "1rem",
    marginTop: "30px",
    minHeight: "32px",
    textDecoration: "none",
    fontSize: "28px",
  },
  description: {
    color: "#999",
    fontSize: "22px",
    fontFamily: "Open Sans"
  },
};

export default productStyle;
