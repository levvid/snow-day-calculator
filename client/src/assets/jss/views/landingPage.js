import { container, title } from "../main-theme.js";

const landingPageStyle = {
  container: {
    zIndex: "12",
    color: "#000000",
    ...container,
  },
  title: {
    ...title,
    display: "inline-block",
    position: "relative",
    marginTop: "10px",
    minHeight: "32px",
    color: "#000000",
    textDecoration: "none",
    fontSize: "35px"
  },
  button: {
    ...title,
    display: "inline-block",
    position: "relative",
    marginTop: "10px",
    minHeight: "32px",
    color: "#FFFFFF",
    textDecoration: "none",
    fontSize: "15px"
  },
  subtitle: {
    fontSize: "1rem",
    fontFamily: "Open Sans",
    // maxWidth: "500px",
    // margin: "10px auto 0",
  },
  main: {
    background: "#FFFFFF",
    position: "relative",
    zIndex: "3",
  },
  mainRaised: {
    margin: "-60px 30px 0px",
    borderRadius: "6px",
    boxShadow:
      "0 16px 24px 2px rgba(0, 0, 0, 0.14), 0 6px 30px 5px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(0, 0, 0, 0.2)",
  },
  bgColor: {
    backgroundColor: "aliceblue !important",
  }
};

export default landingPageStyle;
