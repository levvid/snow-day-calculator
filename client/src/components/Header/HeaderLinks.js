/*eslint-disable*/
import React from "react";
import DeleteIcon from "@material-ui/icons/Delete";
import IconButton from "@material-ui/core/IconButton";
// react components for routing our app without refresh
import { Link } from "react-router-dom";

// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Tooltip from "@material-ui/core/Tooltip";
import { HashLink as HLink } from 'react-router-hash-link';

// @material-ui/icons
import { Apps, CloudDownload } from "@material-ui/icons";

// core components
import Button from "../CustomButtons/Button.js";

import styles from "../../assets/jss/components/headerLinksStyle.js";

const useStyles = makeStyles(styles);

export default function HeaderLinks(props) {
  const classes = useStyles();
  return (
    <List className={classes.list}>
      <ListItem className={classes.listItem}>
        <Link to="/" className={classes.navLink}>
          Home
        </Link>
      </ListItem>
      <ListItem className={classes.listItem} >
        <Link to="/blog" className={classes.navLink}>
          Blog
        </Link>
      </ListItem>
      <ListItem className={classes.listItem}>
        {/* <Link to="/blog/#contact-us" className={classes.navLink}>
          Contact Us
        </Link> */}
        <HLink to="/blog/contact-us" className={classes.navLink}>Contact Us</HLink>

      </ListItem>
      
    </List>
  );
}
