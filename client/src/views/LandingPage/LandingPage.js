import React, { useState, useEffect, useRef } from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
import PropTypes from 'prop-types';

// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import Box from '@material-ui/core/Box';
import Alert from '@mui/material/Alert';
import Collapse from '@mui/material/Collapse';
import TextField from '@mui/material/TextField';
import Autocomplete, { autocompleteClasses, createFilterOptions } from '@mui/material/Autocomplete';
import useMediaQuery from '@mui/material/useMediaQuery';
import ListSubheader from '@mui/material/ListSubheader';
import Popper from '@mui/material/Popper';
import { useTheme, styled } from '@mui/material/styles';
import { VariableSizeList } from 'react-window';
import Typography from '@mui/material/Typography';

import GridContainer from "../../components/Grid/GridContainer.js";
import GridItem from "../../components/Grid/GridItem.js";
import Button from "../../components/CustomButtons/Button.js";
import HeaderLinks from "../../components/Header/HeaderLinks.js";
import CustomInput from "../../components/CustomInput/CustomInput.js";
import Header from "../../components/Header/Header.js";
import Footer from "../../components/Footer/Footer.js";
import CustomTabs from "../../components/CustomTabs/CustomTabs.js";
import ProductSection from "./Sections/ProductSection.js";


import Code from "@material-ui/icons/Code";
import City from "@material-ui/icons/LocationCity";

import axios from 'axios';


import styles from "../../assets/jss/views/landingPage.js";
// import img from "../../assets/img/snow_6.jpg";
// import img from "../../assets/img/snow_6.jpg";
// import img from "../../assets/img/landing-bg.jpg";
const cities = require('../../assets/lib/cities-us-canada.js');
const cityNames = Array.from(new Set(cities.city_names));

// console.log('Cities are: ' + JSON.stringify(cityNames));

const dashboardRoutes = [];

const landingPageStyles = {
  paperContainer: {
      // backgroundImage: `url(${img})`,
      backgroundPosition: "center center",
      alignItems: "center",
      marginLeft: "-8px",
      marginRight: "-8px",
      display: "flex"

      // backgroundSize: "cover",
    
  }
};


const LISTBOX_PADDING = 8; // px

function renderRow(props) {
  const { data, index, style } = props;
  const dataSet = data[index];
  const inlineStyle = {
    ...style,
    top: style.top + LISTBOX_PADDING,
  };

  if (dataSet.hasOwnProperty('group')) {
    return (
      <ListSubheader key={dataSet.key} component="div" style={inlineStyle}>
        {dataSet.group}
      </ListSubheader>
    );
  }

  return (
    <Typography component="li" {...dataSet[0]} noWrap style={inlineStyle}>
      {dataSet[1]}
    </Typography>
  );
}

const OuterElementContext = React.createContext({});


const OuterElementType = React.forwardRef((props, ref) => {
  const outerProps = React.useContext(OuterElementContext);
  return <div ref={ref} {...props} {...outerProps} />;
});

function useResetCache(data) {
  const ref = React.useRef(null);
  React.useEffect(() => {
    if (ref.current != null) {
      ref.current.resetAfterIndex(0, true);
    }
  }, [data]);
  return ref;
}

// Adapter for react-window
const ListboxComponent = React.forwardRef(function ListboxComponent(props, ref) {
  const { children, ...other } = props;
  const itemData = [];
  children.forEach((item) => {
    itemData.push(item);
    itemData.push(...(item.children || []));
  });

  const theme = useTheme();
  const smUp = useMediaQuery(theme.breakpoints.up('sm'), {
    noSsr: true,
  });

  const itemCount = itemData.length;
  const itemSize = smUp ? 36 : 48;

  const getChildSize = (child) => {
    if (child.hasOwnProperty('group')) {
      return 48;
    }

    return itemSize;
  };

  const getHeight = () => {
    if (itemCount > 8) {
      return 8 * itemSize;
    }
    return itemData.map(getChildSize).reduce((a, b) => a + b, 0);
  };

  const gridRef = useResetCache(itemCount);

  return (
    <div ref={ref}>
      <OuterElementContext.Provider value={other}>
        <VariableSizeList
          itemData={itemData}
          height={getHeight() + 2 * LISTBOX_PADDING}
          width="100%"
          ref={gridRef}
          outerElementType={OuterElementType}
          innerElementType="ul"
          itemSize={(index) => getChildSize(itemData[index])}
          overscanCount={5}
          itemCount={itemCount}
        >
          {renderRow}
        </VariableSizeList>
      </OuterElementContext.Provider>
    </div>
  );
});

ListboxComponent.propTypes = {
  children: PropTypes.node,
};

const StyledPopper = styled(Popper)({
  [`& .${autocompleteClasses.listbox}`]: {
    boxSizing: 'border-box',
    '& ul': {
      padding: 0,
      margin: 0,
    },
  },
});


const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: "#3498DB",
    color: theme.palette.common.white,
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));


const StyledTableRow = styled(TableRow)(({ theme }) => ({
  '&:nth-of-type(odd)': {
    backgroundColor: theme.palette.action.hover,
  },
  // hide last border
  '&:last-child td, &:last-child th': {
    border: 0,
  },
}));

const filterOptions = createFilterOptions({
  matchFrom: 'any',
  limit: 500,
});


function createData(percentage, description) {
  return { percentage, description };
}

const useStyles = makeStyles(styles);


export default function LandingPage(props) {
  const classes = useStyles();
  const { ...rest } = props;

  const [zipCode, setZipCode] = useState(0);
  const [city, setCity] = useState('');


  const [snowAmtToday, setSnowAmtToday] = useState(0);
  const [probOfPrecipitationToday, setProbOfPrecipitationToday] = useState(0);
  const [snowDayProb, setSnowDayProb] = useState(0);
  const [calculate, setCalculate] = useState(0);
  const [timeOfPrediction, setTimeOfPrediction] = useState('');
  const [error, setError] = useState(false);
  const [alertType, setAlertType] = useState('warning');
  const [errorMessage, setErrorMessage] = useState('Please enter 5 digit zip code from US or Canada locations.')
  const [tabValue, setTabValue] = useState(0);
  const didMountRef = useRef(false);
  const [showTable, setShowTable] = useState(false);
  const [makeAPICall, setMakeAPICall] = useState(false);
  
  const [autoOpen, setAutoOpen] = useState(false); 


  const calculateSnowDay = (event) => {
    console.log('Submitting form: ' + city);
    setMakeAPICall(true);
    setCalculate(calculate + 1);
  };

  const toTitleCase =(str) => {
    if (!str) return "";
    return str.replace(/\w\S*/g, function(txt){
      return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
  });
  }

  const formatCity = (str) => {
    let res = toTitleCase(str);
    // console.log("city formatted: " + res);
    return res;
  }

  const handleCityChange = (event, value) => {
    console.log('Setting city: ' + JSON.stringify(value));
    // console.log('Value is: ' + value);
    // console.log(cities);
    
    if (value && value.length > 2) {
      console.log('Setting iautoOpen to true');
      setAutoOpen(true);
    } else {
      setAutoOpen(false);
    }
    setCity(value);

    if(cityNames.includes(value)) {
      setAutoOpen(false);
    }
  }

  const handleZipCodeChange = event => {
    setZipCode(event.target.value);
  }


  const handleTabChange = (_, value) => {
    setTabValue(value);
    setMakeAPICall(false);
    setCity('');
    
    if(value === 0) {
      setErrorMessage('Please enter 5 digit zip code from US or Canada locations.');
    } else {
      setErrorMessage('Please enter a valid city name from US or Canada locations.');
    }
  }

  const formatDate = (utcSeconds) => {
    var d = new Date(0); // The 0 there is the key, which sets the date to the epoch
    d.setUTCSeconds(utcSeconds);


    return d.toLocaleDateString("en-US", { weekday: "long"}) + ", " + d.toLocaleDateString("en-US");

  }

  const getCurrentDate = () => {
    var d = new Date();
    return d.toLocaleDateString("en-US", { weekday: "long"}) + ' ' + d.toLocaleString() + ' ' + d.toLocaleTimeString('en-us',{timeZoneName:'short'}).split(' ')[2];
  }

  // Zip code clean up
  const handleZipcodeInputKeyDown = e => {
    

    
  };

  const handleZipcodeInputKeyUp = e => {
    setZipCode(e.target.value);
  };

  const handleZipcodeInputPaste = e => {
    e.preventDefault();

    // get pasted content
    let pasteText = e.clipboardData.getData("text/plain");
    // only allow integers
    pasteText = pasteText.replace(/[^0-9]/g, "");
    // add to current input value (target)
    let newContent = e.target.value + pasteText;
    // only allow 5 digits total
    newContent = newContent.substring(0, 5);
    // set new value of input
    e.target.value = newContent;
    // set new state of zipcode
    setZipCode(newContent);
  };


  useEffect(() => {
    
    setError(false);
    setShowTable(false);
    if (!didMountRef.current) {
      didMountRef.current = true;
      return;
    }

    if(!makeAPICall) {
      return;
    }

      if(zipCode === 0 && city === '') {
        setAlertType('warning');
        if(tabValue === 0) {
          setErrorMessage('Please enter 5 digit zip code from US or Canada locations.');
        } else {
          setErrorMessage('Please enter a valid city name from US or Canada locations.');
        }
        setError(true); // 
        return;
      }

      axios.post('/calculate', {
        zipCode: zipCode,
        city: city,
        tabValue: tabValue
      })
      .then(response => {
        let error = response?.data?.error;

        if(error) {
          setAlertType('error');
          if(tabValue === 0) {
            setErrorMessage('Please enter 5 digit zip code from US or Canada locations.');
          } else {
            setErrorMessage('Please enter a valid city name from US or Canada locaions.');
          }
          setError(true);
        } else {
          setProbOfPrecipitationToday(response?.data?.probOfPrecipitationToday);
          setSnowAmtToday(response?.data?.snowAmtToday);
          setSnowDayProb(response?.data?.snowDayProb);
          setTimeOfPrediction(response?.data?.timeOfPrediction);
          setShowTable(true);
        }
        
      })


  }, [calculate, tabValue]);


  const rows = [
    createData('Amount of snow', snowAmtToday + 'mm'),
    createData('Probability of precipitation', probOfPrecipitationToday + '%'),
    createData('Snow day probability', snowDayProb + '%'),  
  ]


  return (
    <div>
      <Header
        color="transparent"
        routes={dashboardRoutes}
        brand="Snow Day Calculator"
        rightLinks={<HeaderLinks />}
        fixed
        changeColorOnScroll={{
          height: 400,
          color: "white",
        }}
        {...rest}
      />
      <div style={landingPageStyles.paperContainer}>
        <div className={classes.container}>
          <GridContainer xs={12} sm={12} md={8} lg={8}>
            <GridItem  style={{ marginTop:"200px"}}>
              <h1 className={classes.title}>SNOW DAY CALCULATOR</h1>
              <h4 className={classes.subtitle}>
                  Please enter your zip-code or city name below.
              </h4>
              <CustomTabs
                headerColor="primary"
                value={tabValue}
                onChange={handleTabChange}
                tabs={[
                  {
                    tabName: "Enter Zip Code",
                    tabIcon: Code,
                    tabContent: (
                      <Box xs={12} sm={12} md={12} lg={12}>
                        {error && <Collapse in={error}><Alert color="error" onClose={() => {setError(false);}}severity={alertType}>{errorMessage}</Alert></Collapse>}
                        <CustomInput
                            inputmode="numeric"
                            type="number"
                            labelText="Zip Code"
                            id="float"
                            formControlProps={{
                                fullWidth: false
                            }}
                            onChange={handleZipCodeChange}
                            onKeyDown={e => handleZipcodeInputKeyDown(e)}
                            onKeyUp={e => handleZipcodeInputKeyUp(e)}
                            onPaste={e => handleZipcodeInputPaste(e)}
                            pattern="\d*"
                        />
                      </Box>
                    ),
                  },
                  {
                    tabName: "Enter City",
                    tabIcon: City,
                    tabContent: (
                      <Box xs={12} sm={2} md={2} lg={1}>
                        {error && <Collapse in={error}><Alert color="error" onClose={() => {setError(false);}}severity={alertType}>{errorMessage}</Alert></Collapse>}
                      
                        <Autocomplete
                          id="virtualize-demo"
                          sx={{ width: 300 }}
                          inputValue={city}
                          disableListWrap
                          filterOptions={filterOptions}
                          PopperComponent={StyledPopper}
                          ListboxComponent={ListboxComponent}
                          options={cityNames}
                          groupBy={(option) => option[0].toUpperCase()}
                          getOptionLabel={(option) => option.toString()}
                          renderInput={(params) => <TextField 
                            {...params} 
                            label="City Name" 
                            style={{ color: "#FFFFFF"}} 
                            inputProps={{
                              ...params.inputProps,
                              color: "#fff"
                            }}
                          />}
                          renderOption={(props, option) => [props, option]}
                          renderGroup={(params) => params}
                          value={city}
                          onChange={handleCityChange}
                          open={autoOpen}
                          // open={city ? city.length > 2 : false }
                          onInputChange={handleCityChange}
                          // autoComplete={false}
                          style={{
                            marginTop: "20px",
                            marginBottom: "7px"
,                          }}
                          />
                      </Box> 
                    ),
                  },
                  
                ]}
              />
            </GridItem>
            <GridItem style={{ marginBottom: "60px"}}>
              <Button 
                style={{ backgroundColor: "#2471A3" }} 
                className={classes.button}
                onClick={calculateSnowDay}
              >Calculate
              </Button>
            </GridItem>
            
          </GridContainer>
          {showTable && 
            <GridContainer style={{ marginTop: "20px", marginBottom: "40px" }}>
            <GridItem xs={12} sm={12} md={6} lg={6}>
              <TableContainer component={Paper}>
                <Table  aria-label="customized table">
                <TableHead>
                  <TableRow>
                    <StyledTableCell colSpan={2} align="center">Prediction for { tabValue === 0 ? zipCode : formatCity(city) }</StyledTableCell>
                  </TableRow>
                </TableHead>
                  <TableBody>
                  <StyledTableRow>
                    <StyledTableCell colSpan={2} align="center" style={{ fontWeight: "500"}}>Prediction date - { formatDate(timeOfPrediction) }</StyledTableCell>
                  </StyledTableRow>
                    {rows.map((row) => (
                      <StyledTableRow key={row.percentage}>
                        <StyledTableCell component="th"  align="left">
                          {row.percentage}
                        </StyledTableCell>
                        <StyledTableCell align="left">{row.description}</StyledTableCell>
                      </StyledTableRow>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>
              <GridItem style={{ marginTop: "15px",  fontFamily: "Open Sans" }}>Current as of { getCurrentDate() }</GridItem>
            </GridItem>
          </GridContainer>
          }

          <GridContainer classname={classes.bgColor} style={{ background: "aliceblue !important", backgroundColor: "aliceblue !important", marginLeft: "20px  !important", marginRight: "20px !important", fontFamily: "Open Sans", fontSize: "16px", padding: "20px" }}>
          <GridItem >
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ut diam quis mi pretium lobortis. Curabitur sed aliquet metus, vel tempor velit. Nam pulvinar euismod commodo. Proin ac sodales orci. Proin fringilla semper ligula, et accumsan leo facilisis a. Sed pellentesque nisi non odio cursus, quis aliquet est varius. Etiam non quam vitae eros mollis pulvinar id in quam. Vivamus id gravida mi, volutpat porttitor nulla.</GridItem>

          <GridItem style={{marginTop: "20px" }}>In tempus ullamcorper enim, id pretium turpis egestas quis. Quisque commodo luctus sem, elementum ullamcorper velit euismod non. Nulla ultricies enim leo, sit amet tristique augue lacinia ut. Aenean vitae faucibus lorem. Phasellus finibus ex ut augue vehicula, id vulputate velit fermentum. Mauris quis elementum ligula. Quisque ut nibh non nisl rhoncus fringilla. Quisque et tristique nisi, vitae gravida lorem. Nam condimentum luctus elit, a euismod metus. Phasellus eu varius leo. Pellentesque nunc nulla, faucibus eget nulla quis, fermentum volutpat augue. Suspendisse potenti. Nunc eget purus tempus, tincidunt velit non, condimentum eros. Praesent quis arcu in erat porta tincidunt.</GridItem>
          
          </GridContainer>
       
          
        </div> 
      </div>
      <div className={classNames(classes.main)} style={{ display: "flex" }} xs={12} sm={12} md={12} lg={12}>
        <div className={classes.container}>
          <ProductSection />
        </div>
      </div>
        <Footer />
      </div> 
    );
}
