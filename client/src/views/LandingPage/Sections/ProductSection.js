import React from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";

// @material-ui/icons
import Chat from "@material-ui/icons/Opacity";
import VerifiedUser from "@material-ui/icons/AcUnit";
import Fingerprint from "@material-ui/icons/LocalHotel";
// core components
import GridItem from "../../../components/Grid/GridItem.js";
import InfoArea from "../../../components/InfoArea/InfoArea.js";
import GridContainer from "../../../components/Grid/GridContainer.js";

import styles from "../../../assets/jss/views/landingPageSections/productStyle.js";

const useStyles = makeStyles(styles);

export default function ProductSection() {
  const classes = useStyles();
  return (
    <div className={classes.section} style={{ zIndex: "9999"}}>
      <GridContainer justify="center">
        <GridItem xs={12} sm={12} md={8}>
          <h2 className={classes.title}>Snow Day Calculator Features</h2>
          <h5 className={classes.description}>
            List of top features
          </h5>
        </GridItem>
      </GridContainer>
      <div>
        <GridContainer>
          <GridItem xs={12} sm={12} md={4}>
            <InfoArea
              title="Probability of precipitation"
              description="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras non iaculis est. Curabitur lacus nulla, tincidunt quis tortor eu, vehicula malesuada odio. In hac habitasse platea dictumst. Mauris faucibus fermentum faucibus. Phasellus leo lorem, tincidunt et urna nec, interdum euismod magna. Etiam sodales viverra feugiat."
              icon={Chat}
              iconColor="info"
              vertical
            />
          </GridItem>
          <GridItem xs={12} sm={12} md={4}>
            <InfoArea
              title="Amount of snow (mm)"
              description="Vivamus eu congue risus. Nullam ullamcorper tellus feugiat tortor rutrum, id pulvinar sapien porttitor. Proin suscipit vitae dolor sed varius. Ut tempus magna et nunc ullamcorper, in bibendum odio consectetur. Etiam sagittis nisi non semper dictum. Fusce nisl sem, tempor vel vulputate at, luctus et mi. Pellentesque imperdiet, sem a accumsan elementum, tellus mauris blandit turpis, nec egestas magna tellus tristique mi."
              icon={VerifiedUser}
              iconColor="success"
              vertical
            />
          </GridItem>
          <GridItem xs={12} sm={12} md={4}>
            <InfoArea
              title="Probability of snow day"
              description="Etiam sagittis nisi non semper dictum. Fusce nisl sem, tempor vel vulputate at, luctus et mi. Pellentesque imperdiet, sem a accumsan elementum, tellus mauris blandit turpis, nec egestas magna tellus tristique mi. Nullam ultrices, ligula ac tristique efficitur, turpis tortor ornare ante, a scelerisque ex ex ut purus. Donec sollicitudin dapibus tellus, eu finibus velit suscipit vitae. Maecenas interdum convallis neque, ac luctus orci malesuada et."
              icon={Fingerprint}
              iconColor="danger"
              vertical
            />
          </GridItem>
        </GridContainer>
      </div>
    </div>
  );
}
