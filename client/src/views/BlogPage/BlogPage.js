import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// core components
import Header from "../../components/Header/Header.js";
import Footer from "../../components/Footer/Footer.js";
import HeaderLinks from "../../components/Header/HeaderLinks.js";
import ContactSection from "../LandingPage/Sections/ContactSection.js";
import styles from "../../assets/jss/components/blogPage.js";

const useStyles = makeStyles(styles);

export default function BlogPage(props) {
  const classes = useStyles();
  const { ...rest } = props;

  return (
    <div>
      <Header
        color="transparent"
        brand="Snow Day Calculator"
        rightLinks={<HeaderLinks />}
        fixed
        changeColorOnScroll={{
          height: 200,
          color: "white",
        }}
        {...rest}
      />
      
      <div className={classNames(classes.main)}>
      <div>  
    </div>
        <div>
          <div className={classes.container}>

            <ContactSection />
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
}
