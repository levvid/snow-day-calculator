import React, { Component } from "react";
import LandingPage from "./views/LandingPage/LandingPage";
 
class Home extends Component {
  render() {
    return (
      <LandingPage />
    );
  }
}
 
export default Home;