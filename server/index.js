const express = require("express");
const path = require('path');
const axios = require('axios');



const PORT = process.env.PORT || 5000;

const app = express();
const API_KEY = '102a653757bb93d61dff0fa1d15993e6';
const API_KEY_ZIP = '50d8be22fd81bef772fdea06df318f77';


var bodyParser = require('body-parser');
const { time } = require("console");
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json())


// Have Node serve the files for our built React app
app.use(express.static(path.resolve(__dirname, '../client/build')));



app.get("/api", (req, res) => {
    res.json({ message: "Hello from server!" });
});


app.post("/calculate", (req, res) => {
    let userData = req?.body;
    let zipCode = userData.zipCode;
    let city = userData.city;
    let tabValue = userData.tabValue;

    let countryCode = 'us';

    let currentWeatherApiUrl = '';

    if (tabValue === 0) { // zip code
        currentWeatherApiUrl = `https://api.openweathermap.org/data/2.5/weather?zip=${zipCode},${countryCode}&appid=${API_KEY_ZIP}`;
    } else {
        currentWeatherApiUrl = `https://api.openweathermap.org/data/2.5/weather?q=${city},${countryCode}&appid=${API_KEY_ZIP}`
    }
    
    axios.get(currentWeatherApiUrl)
    .then(response => {
        let data = response.data;

        if(data.cod != 200) {
            res.json({ 
                error: 'City not found'
            });
        }


        let lat = data.coord.lat;
        let lon = data.coord.lon;

        let oneCallWeatherApiUrl = `https://api.openweathermap.org/data/2.5/onecall?lat=${lat}&lon=${lon}&exclude=hourly,minutely,current,${countryCode}&appid=${API_KEY}`;
        
        
        axios.get(oneCallWeatherApiUrl)
        .then(response => {
            let data = response.data;

            if(data.cod) {
                res.json({ 
                    error: 'City not found'
                });
                return;
            }

            let today = data.daily[1];

            let probOfPrecipitationToday = today?.pop;
            let snowAmtToday = today?.snow;

            let snowChanceToday = snowAmtToday ? true: false;
            let snowDayProb = 0;
            let timeOfPrediction = today?.dt;

            res.json({ 
                probOfPrecipitationToday: (probOfPrecipitationToday * 100).toFixed(2),
                snowAmtToday: snowAmtToday? snowAmtToday: 0,
                snowDayProb: snowDayProb,
                timeOfPrediction: timeOfPrediction,
            });
        })
        .catch(error => {
            console.log(error);
            console.log('Error');
            res.json({ error: "Error" });
        });
    })
    .catch(error => {
        console.log(error);
        console.log('Error');
        res.json({ error: "Error" });
    });



    
});


app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, '../client/build', 'index.html'));
});

app.listen(PORT, () => {
  console.log(`Server listening on ${PORT}`);
});